#!/bin/bash
g++ -o Bin/pack_prop.o -c pack_prop.cpp
g++ -o Bin/pack_Xion.o -c pack_Xion.cpp

g++ -o Bin/0_i_tl_n.o -c Prop_n/0_i_tl_n.cpp
g++ -o Bin/0_i_tl_n Bin/pack_prop.o Bin/0_i_tl_n.o 

g++ -o Bin/0_i_tl_p.o -c Prop_p/0_i_tl_p.cpp
g++ -o Bin/0_i_tl_p Bin/pack_prop.o Bin/0_i_tl_p.o 

g++ -o Bin/0_i_tl_e.o -c Prop_e/0_i_tl_e.cpp
g++ -o Bin/0_i_tl_e Bin/pack_prop.o Bin/0_i_tl_e.o 

g++ -o Bin/1_EE0max_n.o -c Prop_n/1_EE0max_n.cpp
g++ -o Bin/1_EE0max_n Bin/pack_prop.o Bin/1_EE0max_n.o 

g++ -o Bin/1_EE0max_p.o -c Prop_p/1_EE0max_p.cpp
g++ -o Bin/1_EE0max_p Bin/pack_prop.o Bin/1_EE0max_p.o 

g++ -o Bin/1_EE0max_e.o -c Prop_e/1_EE0max_e.cpp
g++ -o Bin/1_EE0max_e Bin/pack_prop.o Bin/1_EE0max_e.o 

g++ -o Bin/2_diff_fc_n.o -c Prop_n/2_diff_fc_n.cpp
g++ -o Bin/2_diff_fc_n Bin/pack_prop.o Bin/2_diff_fc_n.o 

g++ -o Bin/2_diff_fc_p.o -c Prop_p/2_diff_fc_p.cpp
g++ -o Bin/2_diff_fc_p Bin/pack_prop.o Bin/2_diff_fc_p.o 

g++ -o Bin/2_diff_fc_e.o -c Prop_e/2_diff_fc_e.cpp
g++ -o Bin/2_diff_fc_e Bin/pack_prop.o Bin/2_diff_fc_e.o 

g++ -o Bin/3_ball_fc_n.o -c Prop_n/3_ball_fc_n.cpp
g++ -o Bin/3_ball_fc_n Bin/pack_prop.o Bin/3_ball_fc_n.o 

g++ -o Bin/3_ball_fc_p.o -c Prop_p/3_ball_fc_p.cpp
g++ -o Bin/3_ball_fc_p Bin/pack_prop.o Bin/3_ball_fc_p.o 

g++ -o Bin/3_ball_fc_e.o -c Prop_e/3_ball_fc_e.cpp
g++ -o Bin/3_ball_fc_e Bin/pack_prop.o Bin/3_ball_fc_e.o 

g++ -o Bin/5_ionization.o -c 5_ionization.cpp
g++ -o Bin/5_ionization Bin/pack_Xion.o Bin/5_ionization.o 

g++ -o Bin/6_sputtering.o -c 6_sputtering.cpp
g++ -o Bin/6_sputtering Bin/pack_Xion.o Bin/pack_prop.o Bin/6_sputtering.o 
