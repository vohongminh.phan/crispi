# CRISPI: Cosmic-Ray Intensity for Star formation and Physics of the Interstellar medium

This is a code to inverstigate the transport of cosmic rays into molecular clouds. 

## Getting started

To run the code, you have to first have to create a Bin directory by typing the following in your terminal.

**mkdir Bin**

Now, you have to compile the code by typing the following in your terminal.

**./compile_prop.sh**

(If permision is denied, run the following.

**chmod +x compile_prop.sh**

Then compile the code as above.)

## Cosmic-ray propagation

You can now run the propagation of Voyager spectra into clouds with the execute script.

**python3 4_execute.py**

Note that all the main codes for propagation are stored in "./Prop_p" and "./Prop_e".

## Predictions for ionization rate

The above script will take a while, once it is done you can compute the ionization rate.

**./Bin/5_ionization**

## Plots for ionization rates

And finally plot some results.

**python3 6_plot_prop.py**

Results for the diffusive model (Phan et al. 2018) and the ballistic model (Padovani et al. 2009) are shown in "fg_ion_LocISM_diff.png" and "fg_ion_LocISM_ball.png" respectively.

You could change the ISM spectrum by changing the input spectra from line 161 to 185 of "./Prop_p/2_diff_fc_p.cpp" and "./Prop_e/2_diff_fc_e.cpp" (for the diffusive model). 

And similarly from line 126 to line 150 of "./Prop_p/2_diff_fc_p.cpp" and "./Prop_e/2_diff_fc_e.cpp" (for the ballistic model).   

Notice also to change the input spectra file name accordingly with the string gal_name. Note also that the input file for the spectra should be ordered following the structured in "pars_prop.dat" (see instructions in "pars_prop.dat"). 
