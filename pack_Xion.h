// Electron capture cross-section
double func_sigma_ec(double Ep_p);


/////////////////////////////////////////////////////////////////////////////
// Proton ionization differential cross-section and cross-section
/////////////////////////////////////////////////////////////////////////////

// Auxiliary function for ionization cross-section of protons
double func_F1(double v);

// Auxiliary function for ionization cross-section of protons
double func_F2(double v);

// Differential cross-section of protons (Rudd model)
double func_d_sigma_p_E(double Ep_p, double Ee_s);

// Ionization cross-section for protons
double func_sigma_p(double Ep_p);


/////////////////////////////////////////////////////////////////////////////
// Electron ionization differential cross-section and cross-section
/////////////////////////////////////////////////////////////////////////////

// Auxiliary function for ionization differential cross-section of electrons
double func_df_dw(double w);

// Auxiliary function for ionization cross-section of electrons
double func_D(double t);

// Differential cross-section of electrons (RBEB binary-encounter-Bethe)
double func_d_sigma_e_E(double Ee_p, double Ee_s);

// Ionization cross-section for electrons
double func_sigma_e(double Ee_p);


/////////////////////////////////////////////////////////////////////////////
// Secondary ionization 
/////////////////////////////////////////////////////////////////////////////

// Secondary ionization function for protons
double func_phi_p(double X_phi_p[], double Y_phi_p[], double Ep_p);

// Secondary ionization function for electrons
double func_phi_e(double X_phi_e[], double Y_phi_e[], double Ee_p);
