#include <iostream>
#include <complex>
#include <math.h>
#include <fstream>
#include <string>
#include <iomanip>
#include <ctime>
#include "../pack_prop.h"

using namespace std;

const double mp=938.272e6;// eV
const double me=0.510998e6;// eV

const double m=me;// eV

const double nH20=100.0;// cm^-3
const double ni=1.0e-2; //cm^-3
const double B0=10.0;// uG -> Calibrated magnetic field for the integral file i_tl_p.dat
const double B=10.0;// uG -> Magnetic field 

const double pi=3.14159265359;
const double Lc=10.0;// pc -> Size of the molecular cloud

const double c0=3.0e8*365.0*86400/(3.0857e16);// This is the speed of light pc/yr
const double vA_ISM=2.18e9*(pow(ni,-0.5))*B*1.0e-6*365.0*86400.0/(3.0857e16); // pc/yr -> Alfven speed in the vicinity of the molecular cloud

int main(int argc, char* argv[]){
    
    // Calling the input file for the energy resolution
    ifstream input;
    input.open("pars_prop.dat");
    double Emin, Emax, dlogE, dlogE_smooth;
    input >> Emin >> Emax >> dlogE >> dlogE_smooth;

    // Note that we define 2 energy resolutions 
    // dlogE -> defined for the initial energy files 
    // dlogE_smooth -> defined for the integral files 
    // The integral is carried on to determine the CR spetrum inside clouds at high energy using finner energy bin dlogE_smooth.
    // The difference in the energy resolutions is then defined with bin_diff. 
    int bin_dif=dlogE/dlogE_smooth;// -> The integration at high energy will have bin_dif times more point then the spectrum printed
    int NE=round(log10(Emax/Emin)/dlogE)+1;
	int NE_smooth=round(log10(Emax/Emin)*100.0/(dlogE_smooth*100.0))+1;

    cout << "Number of points in the printed spectrum: " << NE << endl;
    cout << "Number of points in the integrated spectrum: " << NE_smooth << endl;

    // Define input and output 
    ofstream output;
    ifstream input1, input2, inputCR;

    // Define the SBN considered
    string gal_name="LocISM"; 
    
    double nH2;// cm^-3 -> Density of H2 for the molecular cloud

    // Read input arguments from command line.
    if(argc==2){
        nH2=atof(argv[1]);// cm^-3
    } 
    else{
        nH2=200.0;// cm^-3
    }

    cout << " " << endl;
    cout << "Density of the cloud: " << nH2 << " cm^-3" << endl;
    cout << "Column density of the cloud: " << nH2*Lc*3.08567758e18 << " cm^-2" << endl;
    cout << "Alfven speed in the vicinity of the cloud: " << vA_ISM*(3.0857e16)/(365.0*86400.0*1000.0) << " km/s" << endl;

    string pref_o, suf_o, pref_i, suf_i1, suf_i2;
    pref_o="./Prop_e/e_spectrum_";
    suf_o=".dat";
    pref_i="./Prop_e/matrix_EE0_e_smooth";
    suf_i1="_10.dat";
    suf_i2="_5.dat";
    
    string tag_nH2="_"+to_string(int(nH2));
    if(nH2<1.0){
    	tag_nH2="_0p"+to_string(int(nH2*10.0));
    }
    if(nH2<0.1){
    	tag_nH2="_0p0"+to_string(int(nH2*100.0));
    }
    
    cout << " " << endl;
	cout << "Filename for the initial energy: " << (pref_i+tag_nH2+suf_i1).c_str() << endl;

    string output_name=pref_o+gal_name+"_ball"+tag_nH2+".dat";
	output.open(output_name.c_str());

    cout << "Output file is: " << output_name << endl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// IMPORT DATA FOR THE SPECTRUM                                                                                                           //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    int count;
    double E, E0max, dE;

    // E0max for the whole spectrum we want to scan
    input1.open((pref_i+tag_nH2+suf_i1).c_str());

	double *X_EE0=new double[NE];
	double *Y_EE0=new double[NE];
    
    count=0;
    while(input1 >> E >> E0max){
        X_EE0[count]=E;// eV
        Y_EE0[count]=E0max;// eV
        count++;
    }

    // E01 for the whole spectrum we want to scan
    input2.open((pref_i+tag_nH2+suf_i2).c_str());
    
    double *Y_EE01=new double[NE];
    double *Y_EE02=new double[NE];

    count=0;
    while(input2 >> E >> E0max){
        Y_EE01[count]=E0max;// eV
        Y_EE02[count]=E0max;// eV
        count++;
    }

	// Spectrum in the ISM
    double *X_f0=new double[NE];
    double *Y_f0=new double[NE];

    if(gal_name=="LocISM"){
        for(int i=0;i<NE;i++){
            X_f0[i]=Emin*pow(10.0,i*dlogE);// eV
            Y_f0[i]=func_jLocISM_e(X_f0[i])*4.0*pi/(sqrt(pow(X_f0[i]+m,2)-pow(m,2))*3.0e10/(X_f0[i]+m));// eV^-1 cm^-3
        }
    }
    else{
        double Jpp, Jep, Jes, Jet;

        inputCR.open(("CR_spectrum_"+gal_name+"_Emin=1e3.dat").c_str());
        cout << "Filename for the ISM spectrum: " << ("../CR_spectrum_"+gal_name+"_Emin=1e3.dat").c_str() << endl;

        count=0;
        while(inputCR >> E >> Jpp >> Jep >> Jes >> Jet){
            if(count<NE){
                X_f0[count]=Emin*pow(10.0,count*dlogE);// eV
                Y_f0[count]=(Jep+Jes+Jet)*4.0*pi/(sqrt(pow(E+m,2)-pow(m,2))*3.0e10/(E+m));// eV^-1 cm^-3
            }
            count++;
        }
    }

    double *Jp_ism=new double[NE];
    double *Jp_bor=new double[NE];
    double *Jp_mid=new double[NE];
    double *Jp_avg=new double[NE];
    
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// FINDING THE INITIATING ENERGY FOR THE HIGH ENERGY SPECTRUM                                                           //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
	double eta, epsilon, fix=0.0;
	int NE_high=0, m_NE_high=0;

    // This loop is to find the range of E where E0max>E+dE for the high energy spectrum (if E0max<E+dE) the low energy solution would not work
	for(int i=0;i<NE-1;i++){
		E=X_EE0[i];
        dE=X_EE0[i+1]-X_EE0[i];
        
		eta=((Y_EE0[i]-E)/dE)-1.0;
		epsilon=(Y_EE0[i]-E)/E;

		if((fix==0.0) && (eta<0.0)){
			NE_high=NE-i;// Number of points in the high energy spectrum
            m_NE_high=NE-NE_high;// Coordinate of the the point to initiate the high energy spectrum
			fix+=1.0;
		}
	}
    
    // This is to ensure that in the case E0max>E+dE everywhere then we initiate the high energy spectrum at E~3.1 GeV
    if(NE_high==0){
        NE_high=100;
        m_NE_high=NE-NE_high;
    }
    
    cout << " " << endl;
	cout << "Number of point in homogeneous solution: " << NE_high << endl;
	cout << "Initiataing high energy spectrum at: " << X_EE0[m_NE_high] << endl;
	cout << "Corresponding line in file of E0max: " << m_NE_high << endl;
	cout << "Correct up to: (E0max-E)/E=" << (Y_EE0[m_NE_high]-X_EE0[m_NE_high])/X_EE0[m_NE_high] << endl;

    // Define a smoother ism spectrum for the integration at high energy
    double *X_f0_smooth=new double[NE_smooth];
    double *Y_f0_smooth=new double[NE_smooth];

    for(int i=0;i<NE_smooth;i++){
        X_f0_smooth[i]=Emin*pow(10.0,i*dlogE_smooth);
        Y_f0_smooth[i]=func_interp_1D(X_f0,Y_f0,NE,X_f0_smooth[i]);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // THE HIGH ENERGY SPECTRUM CALCULATION                                                                                 //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    double fc;
    double p;
    int cord_E, cord_Emax;
    E=X_f0[m_NE_high];
    
    // To find the coordinate of E in the prepared file of loss time
    cord_E=int(log10(E/Emin)/dlogE_smooth);
    cord_Emax=round(log10(Emax/Emin)/dlogE_smooth);
    
    double *X_fb=new double[NE];
    double *Y_fb=new double[NE];

    for(int i=0;i<NE;i++){
        X_fb[i]=X_f0[i];
        Y_fb[i]=Y_f0[i];
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CALCULATION OF THE SPECTRUM IN THE MIDDLE OF THE CLOUD                                                               //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    double vp;
    double p0max;
    double fcE0max, Lp0max, Lp;

    double E01, p01, E02, p02, Lp01, Lp02, fcE01, fcE02;
    // E01 and E02 are the initial energies from the two sides of the cloud

    double *X_fc=new double[NE];
    double *Y_fc=new double[NE];

    for(int i=0;i<NE;i++){
        X_fc[i]=X_fb[i];
        Y_fc[i]=0.0;
    }

    for(int i=0;i<NE-NE_high;i++){
        fc=0.0;
        E=X_fb[NE-NE_high-i-1];
        p=sqrt(pow(E+m,2)-pow(m,2));
        vp=(p/(E+m))*c0;

        E01=Y_EE01[NE-NE_high-i-1];
        p01=sqrt(pow(E01+m,2)-pow(m,2));	
        E02=Y_EE02[NE-NE_high-i-1];
        p02=sqrt(pow(E02+m,2)-pow(m,2));	
    
        fcE01=func_interp_1D(X_fb,Y_fb,NE,E01);
        Lp01=func_LEe(E01);
        fcE02=func_interp_1D(X_fb,Y_fb,NE,E02);
        Lp02=func_LEe(E02);	
        Lp=func_LEe(E);

        fc+=fcE01*((p01*(E+m)*Lp01)/(p*(E01+m)*Lp));
        fc+=fcE02*((p02*(E+m)*Lp02)/(p*(E02+m)*Lp));
        fc*=1.0/2.0;

        Y_fc[NE-NE_high-i-1]=fc;
    }

    for(int i=0;i<NE_high;i++){
        Y_fc[NE-NE_high+i]=Y_fb[NE-NE_high+i];
    }

    // Print the spectrum in the middle of the cloud.
    for(int i=0;i<NE;i++){
        vp=sqrt(pow(X_fc[i]+m,2)-m*m)*3.0e10/(X_fc[i]+m);
        Jp_mid[i]=Y_fc[i]*vp/(4.0*pi);
    }
    
    // Print the ISM spectrum used but with reduced number of points
    for(int i=0;i<NE;i++){
        vp=sqrt(pow(X_f0_smooth[i*bin_dif]+m,2)-m*m)*3.0e10/(X_f0_smooth[i*bin_dif]+m);
        Jp_ism[i]=Y_f0_smooth[i*bin_dif]*vp/(4.0*pi);
        Jp_bor[i]=Y_f0_smooth[i*bin_dif]*vp/(4.0*pi);
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // CALCULATION OF THE AVERAGE SPECTRUM OF THE CLOUD                                                                     //
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    double vp0, p0, E0, dE0, fbE0;
    
    for(int i=0;i<NE;i++){
        fc=0.0;
        E=X_fb[i];
        p=sqrt(pow(E+m,2)-pow(m,2));
        vp=p/(E+m);
        Lp=func_LEe(E);
        
        E0max=Y_EE0[i];
        E0=E;
        
        while(E0<E0max){
            dE0=min(0.001*E0,E0max-E0);
            
            p0=sqrt(pow(E0+m,2)-pow(m,2));
            vp0=p0/(E0+m);
            fbE0=func_interp_1D(X_fb,Y_fb,NE,E0);
            
            fc+=fbE0*vp0*dE0;
            E0+=dE0;
        }
        
        fc*=1.0/(2.0*nH2*Lp*1.0e-16*Lc*3.0857e18*vp);
        
        if(E0max>1.0e10){
            fc=func_interp_1D(X_fb,Y_fb,NE,E);
        }
        
        vp=sqrt(pow(E+m,2)-m*m)*3.0e10/(E+m);
        Jp_avg[i]=fc*vp/(4.0*pi);
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    for(int l=0;l<NE;l++){
        output << X_EE0[l];
        output << " " << Jp_ism[l];  
        output << " " << Jp_bor[l];  
        output << " " << Jp_mid[l];  
        output << " " << Jp_avg[l];  
        output << endl;
    }

    output.close();
    input.close();
    input1.close();
    input2.close();
    inputCR.close();
}

