#include <iostream>
#include <complex>
#include <math.h>
#include <fstream>
#include <string>
#include <iomanip>
#include "../pack_prop.h"

using namespace std;

const double pi=3.14159265359;
const double me=0.510998e6;// eV

int main(){

    // Calling the input file for the energy resolution
    ifstream input;
    input.open("pars_prop.dat");
    double Emin, Emax, dlogE, dlogE_smooth;
    input >> Emin >> Emax >> dlogE >> dlogE_smooth;

    ofstream output1, output2;   
    output1.open("./Prop_e/i_tl_e.dat");
    output2.open("./Prop_e/tl_smooth_e.dat");
    
    // Normalized cloud density for the loss time and the integral
    double nH20=100.0;// cm^-3

    double E, LE, dE;
    double p, vp, tl;
        
    int NE_smooth=round(log10(Emax/Emin)*100.0/(dlogE_smooth*100.0))+1;
    cout << "Number of points: " << NE_smooth << endl;
    
    double integrate=0.0;
    for(int i=0;i<NE_smooth-1;i++){
        dE=Emin*(pow(10.0,(i+1)*dlogE_smooth)-pow(10.0,i*dlogE_smooth));
        E=Emin*pow(10.0,i*dlogE_smooth);
        p=sqrt(pow(E+me,2)-me*me);
        tl=p/(nH20*3.0e10*func_LEe(E)*1.0e-16*365.0*86400.0);// yr
        
        integrate+=tl*(E+me)*dE/pow(p,2);// yr
    }
    
    for(int i=0;i<NE_smooth-1;i++){
        dE=Emin*(pow(10.0,(i+1)*dlogE_smooth)-pow(10.0,i*dlogE_smooth));
        E=Emin*pow(10.0,i*dlogE_smooth);
        p=sqrt(pow(E+me,2)-me*me);
        tl=p/(nH20*3.0e10*func_LEe(E)*1.0e-16*365.0*86400.0);// yr
        
        output1 << E << " " << setprecision(20) << integrate << endl;
        output2 << E << " " << setprecision(20) << tl << endl;
        
        integrate+=-tl*(E+me)*dE/pow(p,2);// yr
    }
    
    E=Emax;
    p=sqrt(pow(E+me,2)-me*me);
    tl=p/(nH20*3.0e10*func_LEe(E)*1.0e-16*365.0*86400.0);// yr
    
    output1 << Emax << " " << setprecision(20) << 0.0 << endl;
    output2 << Emax << " " << setprecision(20) << tl << endl;
    
    output1.close();
    output2.close();
    input.close();
}


