#include <string>
#include <sstream>
#include <iostream>
#include <math.h>
#include <fstream>
#include <iomanip>
#include <stdio.h>
#include <vector>
#include <string>
#include "pack_prop.h"

using namespace std;

const double mp=938.272e6;// eV
const double me=0.510998e6;// eV

// Energy loss function in a neutral medium made up mostly by H2 for protons -> Works only for En >~ 10^5 eV
// L(E)=(dE/dt)/(nH2*v) -> eV cm^2
double func_LEn(double Z, double A, double En){
// E (eV)
    
    double beta, gamma, bEp=0.0;
    beta=sqrt(pow(En+mp,2)-mp*mp)/(En+mp);
    gamma=(En+mp)/mp;
    
    if(Z>1.0){
        Z*=1.0-1.034*exp(-137.0*beta*pow(Z,-0.688));
    }

    // Ionization loss rate
    // Note that I changed from using the formulae Schlickeiser 2002 to my own fit to avoid the error in Schlickeiser's formula below E~10^4 eV.  
    bEp+=Z*Z*1.81e-7*pow(beta,2)/(6.67e-6+pow(beta,2.744));

    // Proton-proton interaction loss rate
    // Note that the loss function file from Padovani et al. 2009 might have underestimate this function
    // in the energy range dominated by proton-proton interactions. The loss function in this formulae is about 1.7 
    // times lower than that of Padovani et al. 2009 for E >~ 10^9 eV.
    bEp+=pow(A,0.79)*3.85e-7*pow(En/1.0e9,1.28)*pow((En/1.0e9)+200.0,-0.2);

    return bEp*1.0e16/(0.5*beta*3.0e10);// 10^-16 eV cm^2
}

// Energy loss function in a neutral medium made up mostly by H2 for protons -> Works only for E >~ 10^3 eV
// L(E)=(dE/dt)/(nH2*v) -> eV cm^2
double func_LEp(double E){
// E (eV)
    
    double beta, gamma, bEp=0.0;
    beta=sqrt(pow(E+mp,2)-mp*mp)/(E+mp);
    gamma=(E+mp)/mp;
    
    // Ionization loss rate
    // Note that I changed from using the formulae Schlickeiser 2002 to my own fit to avoid the error in Schlickeiser's formula below E~10^4 eV.  
    bEp+=1.81e-7*pow(beta,2)/(6.67e-6+pow(beta,2.744));

    // Proton-proton interaction loss rate
    // Note that the loss function file from Padovani et al. 2009 might have underestimate this function
    // in the energy range dominated by proton-proton interactions. The loss function in this formulae is about 1.7 
    // times lower than that of Padovani et al. 2009 for E >~ 10^9 eV.
    bEp+=3.85e-7*pow(E/1.0e9,1.28)*pow((E/1.0e9)+200.0,-0.2);

    return bEp*1.0e16/(0.5*beta*3.0e10);// 10^-16 eV cm^2
}

// Energy loss function in a neutral medium made up mostly by H2 for electrons -> Works only for E >~ 10^3 eV
// L(E)=(dE/dt)/(nH2*v) -> eV cm^2
double func_LEe(double E){
// E (eV)
    
    double B0=10.0, Urad=1.0;

    double beta, gamma, bEe=0.0;
    beta=sqrt(pow(E+me,2)-me*me)/(E+me);
    gamma=(E+me)/me;
    
    // Ionization loss rate
    bEe+=7.65e-9*(23.31+1.2*log(pow(gamma,3)*pow(beta,4)/(1.0+gamma)))/beta;

    // Bremsstrahlung loss rate
    bEe+=5.2e-10*gamma;

    // // Synchrotron and inverse Compton loss rate
    // bEe+=9.9e-16*pow(gamma,2)*(pow(B0,2)+26.84*Urad);

    return bEe*1.0e16/(0.5*beta*3.0e10);// 10^-16 eV cm^2
}

// Function to solve for the initial particle's energy at the cloud border using Runge-Kutta 4 for protons
double func_En(double Z, double A, double Enin, double lin, double nH2){
    
    double k1, k2, k3, k4;
    double l=lin;
    double En=Enin;// eV
    
    l*=2.0;// This is because the average pitch anlge is mu_avg=1/2. -> The path length is 2 times the size of the cloud.
    double dl=l/1000.0;
    
    int N=int(l/dl)+1;
    
    for (int i=0;i<N;i++){
        dl=min(dl,l);

        // Notice the factor 3.0857e2 is because the loss function L(E) is in unit 10^-16 eV cm^2 and we use l in pc.
        k1=dl*nH2*func_LEn(Z,A,En)*3.0857e2/A;
        k2=dl*nH2*func_LEn(Z,A,En+(k1/2.0))*3.0857e2/A;
        k3=dl*nH2*func_LEn(Z,A,En+(k2/2.0))*3.0857e2/A;
        k4=dl*nH2*func_LEn(Z,A,En+k3)*3.0857e2/A;
        En+=(1.0/6.0)*(k1+2.0*k2+2.0*k3+k4);
        
        l+=-dl;
    }

    // Note that we estimate the initial energy per nucleon.    
    return En;// eV
}

// Function to solve for the initial particle's energy at the cloud border using Runge-Kutta 4 for protons
double func_Ep(double Ein, double lin, double nH2){
    
    double k1, k2, k3, k4;
    double l=lin;
    double E=Ein;// eV
    
    l*=2.0;// This is because the average pitch anlge is mu_avg=1/2. -> The path length is 2 times the size of the cloud.
    double dl=l/1000.0;
    
    int N=int(l/dl)+1;
    
    for (int i=0;i<N;i++){
        dl=min(dl,l);

        // Notice the factor 3.0857e2 is because the loss function L(E) is in unit 10^-16 eV cm^2 and we use l in pc.
        k1=dl*nH2*func_LEp(E)*3.0857e2;
        k2=dl*nH2*func_LEp(E+(k1/2.0))*3.0857e2;
        k3=dl*nH2*func_LEp(E+(k2/2.0))*3.0857e2;
        k4=dl*nH2*func_LEp(E+k3)*3.0857e2;
        E+=(1.0/6.0)*(k1+2.0*k2+2.0*k3+k4);
        
        l+=-dl;
    }
    
    return E;
}

// Function to solve for the initial particle's energy at the cloud border using Runge-Kutta 4 for electrons
double func_Ee(double Ein, double lin, double nH2){
    
    double k1, k2, k3, k4;
    double l=lin;
    double E=Ein;// eV
    
    l*=2.0;// This is because the average pitch anlge is mu_avg=1/2. -> The path length is 2 times the size of the cloud.
    double dl=l/1000.0;
    
    int N=int(l/dl)+1;
    
    for (int i=0;i<N;i++){
        dl=min(dl,l);

        // Notice the factor 3.0857e2 is because the loss function L(E) is in unit 10^-16 eV cm^2 and we use l in pc.
        k1=dl*nH2*func_LEe(E)*3.0857e2;
        k2=dl*nH2*func_LEe(E+(k1/2.0))*3.0857e2;
        k3=dl*nH2*func_LEe(E+(k2/2.0))*3.0857e2;
        k4=dl*nH2*func_LEe(E+k3)*3.0857e2;
        E+=(1.0/6.0)*(k1+2.0*k2+2.0*k3+k4);
        
        l+=-dl;
    }
    
    return E;
}

// Function to interpolate the functions y=fi(x) 
double func_interp_1D(double X_fi[], double Y_fi[], int NE, double E){

    double dlogE=log10(X_fi[1]/X_fi[0]);
    int beg=int(log10(E/X_fi[0])/dlogE);
    if(beg>=NE-1){
        beg=NE-2;
    }
    if(beg<0){
        beg=0;
    }

    double fi=0.0;
    double X1, X2, Y1, Y2;
    X1=X_fi[beg];
    X2=X_fi[beg+1];
    Y1=Y_fi[beg];
    Y2=Y_fi[beg+1];
    
    double A=log(Y2/Y1)/log(X2/X1);
    fi=Y2*pow(E/X2,A);

    if((Y1<=0.0) || (Y2<=0.0)){
        fi=0.0;
    }
    
    return fi;
}

// Fit of the Voyager spectrum for irons 
double func_jLocISM_Fe(string gal_name, double E){ 
    
    double C=7.444e-14;
    double alpha=0.337;
    double beta=2.927;
    double E0=660.55e6;
    
    double f0=0.0;
    if(gal_name=="LocISM"){
        f0=C*pow(E/1.0e6,alpha)*pow(1.0+(E/E0),-beta);
    }
    if(gal_name=="ScaleP"){
        f0=713.0e-6*func_jLocISM_p(E);
    }

    return f0;// eV^-1 cm^-2 s^-1 sr^-1
}

// Fit of the Voyager spectrum for protons 
double func_jLocISM_p(double E){ 
    
    double C=1.882e-9;
    double alpha=0.129056;
    double beta=2.82891;
    double E0=624.5e6;
    
    // double f0=C*pow(E/1.0e6,alpha)*pow(1.0+(E/E0),-beta);
    double f0=12.5e-10*pow(E/1.0e6,0.35)/((1.0+pow(E/80.0e6,1.3))*pow(1.0+pow(E/2.2e9,1.9/2.1),2.1));

    return f0;// eV^-1 cm^-2 s^-1 sr^-1
}

// Fit of the Voyager spectrum for electrons 
double func_jLocISM_e(double E){
    
    double C=4.658e-7;
    double alpha=-1.236;
    double beta=2.033;
    double E0=736.2e6;
        
    double f0=C*pow(E/1.0e6,alpha)*pow(1.0+(E/E0),-beta);
    
    return f0;// eV^-1 cm^-2 s^-1 sr^-1
}

// Stopping power of cosmic-ray irons
double func_Se(double En){

    double SeFe=0.0;
    if(En>1.0e9){
        En=1.0e9;
    }

    SeFe=pow(10.0,2.6*exp(-0.5*pow(log10(En*1.0e-6)-0.1138,2)/pow(2.1192,2)));// eV / 1015 atm
    SeFe*=9.27e-23*1025.0*1033.27;// eV cm^2
    SeFe*=30.0;// Scale factor from the SRIM data in Arslan et al. 2023

    return SeFe;// eV cm^2
}

