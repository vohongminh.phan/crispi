// Energy loss function in a neutral medium made up mostly by H2 for protons -> Works only for En >~ 10^5 eV
// L(E)=(dE/dt)/(nH2*v) -> eV cm^2
double func_LEn(double Z, double A, double En);

// Energy loss function in a neutral medium made up mostly by H2 for protons -> Works only for E >~ 10^3 eV
// L(E)=(dE/dt)/(nH2*v) -> eV cm^2
double func_LEp(double E);

// Energy loss function in a neutral medium made up mostly by H2 for electrons -> Works only for E >~ 10^3 eV
// L(E)=(dE/dt)/(nH2*v) -> eV cm^2
double func_LEe(double E);

// Function to solve for the initial particle's energy at the cloud border using Runge-Kutta 4 for protons
double func_En(double Z, double A, double Ein, double lin, double nH2);

// Function to solve for the initial particle's energy at the cloud border using Runge-Kutta 4 for protons
double func_Ep(double Ein, double lin, double nH2);

// Function to solve for the initial particle's energy at the cloud border using Runge-Kutta 4 for electrons
double func_Ee(double Ein, double lin, double nH2);

// Function to interpolate the functions y=fi(x) 
double func_interp_1D(double X_fi[], double Y_fi[], int NE, double E);

// Fit of the Voyager spectrum for irons 
double func_jLocISM_Fe(std::string gal_name, double E);

// Fit of the Voyager spectrum for protons 
double func_jLocISM_p(double E);

// Fit of the Voyager spectrum for electrons 
double func_jLocISM_e(double E);

// Stopping power of cosmic-ray irons
double func_Se(double En);
