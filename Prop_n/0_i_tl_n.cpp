#include <iostream>
#include <complex>
#include <math.h>
#include <fstream>
#include <string>
#include <iomanip>
#include "../pack_prop.h"

using namespace std;

const double pi=3.14159265359;
const double mp=938.272e6;// eV

int main(){

    // Calling the input file for the energy resolution
    ifstream input;
    input.open("pars_prop.dat");
    double Enmin, Enmax, dlogE, dlogE_smooth;
    double Z, A;

    input >> Enmin >> Enmax >> dlogE >> dlogE_smooth;
    input >> Z >> A;

    double Emin=A*Enmin, Emax=A*Enmax;

    ofstream output1, output2;
    output1.open("./Prop_n/i_tl_Z="+to_string(int(Z))+".dat");
    output2.open("./Prop_n/tl_smooth_Z="+to_string(int(Z))+".dat");

    // Normalized cloud density for the loss time and the integral
    double nH20=100.0;// cm^-3

    double LE, dE;
    double E, p, vp, tl;
    
    int NE_smooth=round(log10(Emax/Emin)/(dlogE_smooth))+1;
    cout << "Number of points: " << NE_smooth << endl;
    
    double integrate=0.0;
    for(int i=0;i<NE_smooth-1;i++){
        dE=Emin*(pow(10.0,(i+1)*dlogE_smooth)-pow(10.0,i*dlogE_smooth));
        E=Emin*pow(10.0,i*dlogE_smooth);
        p=sqrt(pow(E+A*mp,2)-A*A*mp*mp);
        tl=p/(nH20*3.0e10*func_LEn(Z,A,E/A)*1.0e-16*365.0*86400.0);// yr
        
        integrate+=tl*(E+A*mp)*dE/pow(p,2);// yr
    }
    
    for(int i=0;i<NE_smooth-1;i++){
        dE=Emin*(pow(10.0,(i+1)*dlogE_smooth)-pow(10.0,i*dlogE_smooth));
        E=Emin*pow(10.0,i*dlogE_smooth);
        p=sqrt(pow(E+A*mp,2)-A*A*mp*mp);
        tl=p/(nH20*3.0e10*func_LEn(Z,A,E/A)*1.0e-16*365.0*86400.0);// yr
        
        output1 << E/A << " " << setprecision(20) << integrate << endl;
        output2 << E/A << " " << setprecision(20) << tl << endl;
        
        integrate+=-tl*(E+A*mp)*dE/pow(p,2);// yr
    }
    
    E=Emax;
    p=sqrt(pow(E+A*mp,2)-A*A*mp*mp);
    tl=p/(nH20*3.0e10*func_LEn(Z,A,E/A)*1.0e-16*365.0*86400.0);// yr
    
    output1 << Emax/A << " " << setprecision(20) << 0.0 << endl;
    output2 << Emax/A << " " << setprecision(20) << tl << endl;
    
    output1.close();
    output2.close();
    input.close();
}


