#include <iostream>
#include <complex>
#include <math.h>
#include <fstream>
#include <string>
#include <iomanip>
#include "../pack_prop.h"

using namespace std;

const double pi=3.14159265359;
const double Lc=10.0;// pc

int main(int argc, char* argv[]){
    
    // Calling the input file for the energy resolution
    ifstream input;
    input.open("pars_prop.dat");
    double Enmin, Enmax, dlogE, dlogE_smooth;
    double Z, A;

    input >> Enmin >> Enmax >> dlogE >> dlogE_smooth;
    input >> Z >> A;

    double Emin=A*Enmin, Emax=A*Enmax;

    ofstream output1, output2;
    string filename, pref_o1, suf_o1, pref_o2, suf_o2;
    
    pref_o1="./Prop_n/matrix_EE0_Z="+to_string(int(Z))+"_smooth_";
    suf_o1="_10.dat";
    pref_o2="./Prop_n/matrix_EE0_Z="+to_string(int(Z))+"_smooth_";
    suf_o2="_5.dat";
        
    int i;
    double nH2;
    // Read input arguments from command line.
    if(argc==2){
        nH2=atof(argv[1]);// cm^-3
    } 
    else{
        nH2=200.0;// cm^-3
    }
    
    filename=to_string(int(nH2));
    if(nH2<1.0){
    	filename="0p"+to_string(int(nH2*10.0));
    }
    if(nH2<0.1){
    	filename="0p0"+to_string(int(nH2*100.0));
    }

    output1.open((pref_o1+filename+suf_o1).c_str());
    output2.open((pref_o2+filename+suf_o2).c_str());
            
    double p, E, LE;
    int NE, count;
    
    dlogE*=10.0;
    NE=round(log10(Enmax/Enmin)/dlogE)+1;

    double *X_En10=new double[NE];
    double *Y_En10=new double[NE];
    double *X_En5=new double[NE];
    double *Y_En5=new double[NE];
    
    count=0;
    for(int j=0;j<NE;j++){
        E=Emin*pow(10.0,j*dlogE);

        X_En10[count]=E/A;
        Y_En10[count]=func_En(Z,A,E/A,Lc,nH2);
        X_En5[count]=E/A;
        Y_En5[count]=func_En(Z,A,E/A,Lc/2.0,nH2);

        count+=1;
    }
    
    dlogE*=0.1;
    NE=round(log10(Emax/Emin)/dlogE)+1;
    cout << "Number of points: " << NE << endl;
    
    for(int i=0;i<NE;i++){
        E=Emin*pow(10.0,i*dlogE);
        
        output1 << E/A << " " << setprecision(20) << func_interp_1D(X_En10,Y_En10,NE,E/A) << endl;
        output2 << E/A << " " << setprecision(20) << func_interp_1D(X_En5,Y_En5,NE,E/A) << endl;
    }
    
    cout << "Density of the cloud: " << nH2 << " cm^-3" << endl;
    
    output1.close();
    output2.close();
    input.close();
}
