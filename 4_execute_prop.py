import os
nH=(0.2, 0.5, 1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0, 200.0, 500.0, 1000.0, 2000.0)
N_nH=len(nH)

# os.system('./Bin/0_i_tl_n')
# os.system('./Bin/0_i_tl_p')
# os.system('./Bin/0_i_tl_e')

# for i in range (0, N_nH):
#     os.system('./Bin/1_EE0max_n '+str(nH[i]))
#     # os.system('./Bin/1_EE0max_p '+str(nH[i]))
#     # os.system('./Bin/1_EE0max_e '+str(nH[i]))
#     print(' ')

for i in range (0, N_nH):
    print('Diffusive model '+str(nH[i]))
    os.system('./Bin/2_diff_fc_n '+str(nH[i]))
    # os.system('./Bin/2_diff_fc_p '+str(nH[i]))
    # os.system('./Bin/2_diff_fc_e '+str(nH[i]))
    print(' ')

for i in range (0, N_nH):
    print('Ballistic model '+str(nH[i]))
    os.system('./Bin/3_ball_fc_n '+str(nH[i]))
    # os.system('./Bin/3_ball_fc_p '+str(nH[i]))
    # os.system('./Bin/3_ball_fc_e '+str(nH[i]))
    print(' ')
