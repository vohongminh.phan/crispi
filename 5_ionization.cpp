#include <iostream>
#include <complex>
#include <math.h>
#include <fstream>
#include <string>
#include <iomanip>
#include "pack_Xion.h"

using namespace std;

const double pi=3.14159265359;
const double Lc=10.0;// pc

int main(int argc, char* argv[]){

	ifstream input;
    input.open("pars_prop.dat");
    double Emin, Emax, dlogE, dlogE_smooth;
    input >> Emin >> Emax >> dlogE >> dlogE_smooth;

    int NE=int(log10(Emax/Emin)/dlogE)+1;

	// Array to store the CR spectra.
    double *Y_Jp=new double[NE];
    double *Y_Je=new double[NE];

    // Array to store cross sections.
    double *arr_sigma_p=new double[NE];
    double *arr_sigma_e=new double[NE];
    double *arr_phi_p=new double[NE];
    double *arr_phi_e=new double[NE];

    string gal_name="LocISM";

    string pref_ip, pref_ie, suf_i, pref_o1, pref_o2, suf_o;
	pref_ip="./Prop_p/p_spectrum";
	pref_ie="./Prop_e/e_spectrum";
    suf_i=".dat";
    pref_o1="CR_xi";
    pref_o2="d_CR_xi";
    suf_o=".dat";
    
	int N_nH2=13;
    double nH2[13]={0.2, 0.5, 1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0, 200.0, 500.0, 1000.0, 2000.0};
    
    string tag_nH2, tag_mod;

	// Read input arguments from command line.
    if(argc==2){
        tag_mod=argv[1];// Input diff or ball
    } 
    else{
        tag_mod="diff";
    }

	cout << "Model for CR transport in clouds: " << tag_mod << endl;
	tag_mod="_"+tag_mod;
      
    ifstream inputCR_p[N_nH2], inputCR_e[N_nH2];
	string inputCR_p_name, inputCR_e_name; 

	ofstream output1, output2, output3, output4;
    string output1_name, output2_name, output3_name, output4_name;
	output1_name=pref_o1+"_"+gal_name+tag_mod+".dat";
	output2_name=pref_o2+"_"+gal_name+tag_mod+"_20.dat";
	output3_name=pref_o2+"_"+gal_name+tag_mod+"_200.dat";
	output4_name=pref_o2+"_"+gal_name+tag_mod+"_2000.dat";

	output1.open(output1_name.c_str());
	output2.open(output2_name.c_str());
	output3.open(output3_name.c_str());
	output4.open(output4_name.c_str());
    	
	// Secondary ionization function.
    ifstream input_p, input_e;
	input_p.open("phi_p.dat");
	input_e.open("phi_e.dat");

    double E, phi_p, phi_e;
    int N_phi=811, count;

	double *X_phi=new double[N_phi];
	double *Y_phi_p=new double[N_phi];
	double *Y_phi_e=new double[N_phi];

    count=0;
    while(input_p >> E >> phi_p){
        X_phi[count]=E;
        Y_phi_p[count]=phi_p;
        count+=1;
    }

    count=0;
    while(input_e >> E >> phi_e){
        X_phi[count]=E;
        Y_phi_e[count]=phi_e;
        count+=1;
    }

	for(int i=0;i<NE;i++){
		E=Emin*pow(10.0,i*dlogE);
		arr_sigma_p[i]=func_sigma_p(E);
		arr_sigma_e[i]=func_sigma_e(E);
		arr_phi_p[i]=func_phi_p(X_phi,Y_phi_p,E);
		arr_phi_e[i]=func_phi_e(X_phi,Y_phi_e,E);
		if(E>6.0e13){
			arr_sigma_p[i]=0.0;// The formula is not applicable at very high energy?
			arr_sigma_e[i]=0.0;// The formula is not applicable at very high energy?
		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	double dE, J0p, Jbp, Jcp, Jap, J0e, Jbe, Jce, Jae;// E_p is the primary proton kinetic energy
	double xi_H2_p, xi_H2_e, sigma_p, sigma_e;

	cout << "The file name: " << endl;
	cout << " " << endl;

	for(int n=0;n<N_nH2;n++){
		tag_nH2=to_string(int(nH2[n]));
		if(nH2[n]<1.0){
    		tag_nH2="0p"+to_string(int(nH2[n]*10.0));
	    }
		if(nH2[n]<0.1){
    		tag_nH2="0p0"+to_string(int(nH2[n]*100.0));
	    }
		
		inputCR_p_name=pref_ip+"_"+gal_name+tag_mod+"_"+tag_nH2+".dat";
		inputCR_e_name=pref_ie+"_"+gal_name+tag_mod+"_"+tag_nH2+".dat";

		cout << inputCR_p_name << endl; 
		cout << inputCR_e_name << endl;
		cout << " " << endl;

		// Spectrum of CRs in the enrgy range from 10^3 to 10^10    
		inputCR_p[n].open(inputCR_p_name.c_str());
		inputCR_e[n].open(inputCR_e_name.c_str());
		
		count=0;
	    while(inputCR_p[n] >> E >> J0p >> Jbp >> Jcp >> Jap){
        	Y_Jp[count]=Jap;// eV^-1 cm^-2 s^-1 sr^-1
        	count+=1;
    	}

		count=0;
		while(inputCR_e[n] >> E >> J0e >> Jbe >> Jce >> Jae){
        	Y_Je[count]=Jae;// eV^-1 cm^-2 s^-1 sr^-1
        	count+=1;
    	}

		xi_H2_p=0.0;
		xi_H2_e=0.0;

		for(int i=0;i<NE-1;i++){
			E=Emin*pow(10.0,i*dlogE);
			dE=Emin*(pow(10.0,(i+1)*dlogE)-pow(10.0,i*dlogE));
			
			// Intensity of CRs with respect to kinetic energy.
			Jap=Y_Jp[i];// eV^-1 cm^-2 s^-1 sr^-1 
			Jae=Y_Je[i];// eV^-1 cm^-2 s^-1 sr^-1 

			// Cross-sections and secondary ionization.
			sigma_p=arr_sigma_p[i];// cm^2
			sigma_e=arr_sigma_e[i];// cm^2
			phi_p=arr_phi_p[i];
			phi_e=arr_phi_e[i];

            // Note that Jp is in unit eV^-1 cm^-2 s^-1 sr^-1.
            xi_H2_p+=4.0*pi*Jap*((1.0+phi_p)*sigma_p)*dE;
            xi_H2_e+=4.0*pi*Jae*((1.0+phi_e)*sigma_e)*dE;

			if(nH2[n]==20.0){		
				output2 << E;
				output2 << " " << 4.0*pi*E*Jap*((1.0+phi_p)*sigma_p);
				output2 << " " << 4.0*pi*E*Jae*((1.0+phi_p)*sigma_e);
				output2 << endl;
			}

			if(nH2[n]==200.0){		
				output3 << E;
				output3 << " " << 4.0*pi*E*Jap*((1.0+phi_p)*sigma_p);
				output3 << " " << 4.0*pi*E*Jae*((1.0+phi_p)*sigma_e);
				output3 << endl;
			}

			if(nH2[n]==2000.0){		
				output4 << E;
				output4 << " " << 4.0*pi*E*Jap*((1.0+phi_p)*sigma_p);
				output4 << " " << 4.0*pi*E*Jae*((1.0+phi_p)*sigma_e);
				output4 << " " << sigma_p << " " << phi_p;
				output4 << endl;
			}
		}

	    output1 << nH2[n]*Lc*3.08567758e18 << " " << xi_H2_p << " " << xi_H2_e << endl;

		inputCR_p[n].close();
		inputCR_e[n].close();
	}
      
	output1.close();
    output2.close();
	output3.close();
    output4.close();
	input.close();
    input_p.close();
    input_e.close();
}
