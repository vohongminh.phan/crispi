#include <string>
#include <sstream>
#include <iostream>
#include <math.h>
#include <fstream>
#include <iomanip>
#include <stdio.h>
#include <vector>
#include <string>
#include "pack_Xion.h"

using namespace std;


// NOTE that to switch from proton to electron
//	-> m=me;
//	-> output file phi_e.dat
//	-> primary cross-section sigma_p=func_sigma_e(E_p)	
//	-> primary differential cross-section d_sigma_p=func_d_sigma_e_E(E_p,E_s)

const double mp=938.272e6;// eV
const double me=0.510998e6;// eV
const double m=mp;

const double Lc=10.0;// pc
const double pi=3.14159265359;

const double I=15.603;// eV
const double R=13.6;// eV
const double w2=R/(4.0*I); 	
const double alpha=0.87;// Parameter of the Rudd model (to be distinguish with the fine structure constant) 
const double alpha_f=1/137.035999074;// Fine structure constant
const double a0=0.52917721092*pow(10.0,-8);// cm
const double N=2.0;
const double Sp=4.0*pi*pow(a0,2)*N*pow(R/I,2);
const double lambda=(2.0*mp+2.0*me)/me;
const double Se=4.0*pi*pow(a0,2)*N*pow(alpha_f,4);


// Electron capture cross-section
double func_sigma_ec(double Ep_p){
    
    double x, d_0, d_1, d_2, d_3, d_4, d_5;
    d_0=-52.793928;
    d_1=41.219156;
    d_2=-17.304947;
    d_3=3.292795;
    d_4=-0.238372;
    
    x=Ep_p/((mp/me)*I);
    
    double sigma_ec=pow(10.0,(d_0+d_1*log10(Ep_p)+d_2*pow(log10(Ep_p),2)+d_3*pow(log10(Ep_p),3)+d_4*pow(log10(Ep_p),4)));
    
    return sigma_ec;// cm^2
}


/////////////////////////////////////////////////////////////////////////////
// Proton ionization differential cross-section and cross-section
/////////////////////////////////////////////////////////////////////////////

// Auxiliary function for ionization cross-section of protons
double func_F1(double v){

	double A1, B1, C1, D1, E1;
	A1=0.8;
	B1=2.9;
	C1=0.86;
	D1=1.48;
	E1=7.0;
	
	double L1=C1*pow(v,D1)/(1.0+E1*pow(v,D1+4));
	double H1=A1*log(1.0+pow(v,2))/(pow(v,2)+B1/pow(v,2.0));

	double F1=L1+H1;
	return F1;
}

// Auxiliary function for ionization cross-section of protons
double func_F2(double v){
	
	double A2, B2, C2, D2;
	A2=1.06;
	B2=4.2;
	C2=1.39;
	D2=0.48;

	double L2=C2*pow(v,D2);
	double H2=(A2/pow(v,2))+(B2/pow(v,4));

	double F2=L2*H2/(L2+H2);	
	return F2;
}

// Differential cross-section of protons (Rudd model)
double func_d_sigma_p_E(double Ep_p, double Ee_s){

	double p, d_sigma_E, w, wc, v, v_rel, T;
	p=sqrt(pow(Ep_p+mp,2)-pow(mp,2));
	w=Ee_s/I;
		
	v_rel=p/sqrt(pow(p,2)+pow(mp,2));
	v=sqrt(2.0*mp*me/(I*(2.0*mp+2.0*me)))*v_rel;
	T=Ep_p/lambda;
	wc=((4.0*Ep_p-2.0*sqrt(I*T))/I)-w2;

	d_sigma_E=(Sp/I)*(func_F1(v)+func_F2(v)*w)*pow(1.0+w,-3)/(1.0+exp(alpha*(w-wc)/v));

	return d_sigma_E;
}

// Ionization cross-section for protons
double func_sigma_p(double Ep_p){
	
	double p, sigma_p, w, dw, wmax, wc, T, Qmax, v, v_rel, gamma;
	p=sqrt(pow(Ep_p+mp,2)-pow(mp,2));// eV
	w=0.0;

	v_rel=p/sqrt(pow(p,2)+pow(mp,2));// Dimensionless -> p in eV and Ep_tot in eV
	gamma=1.0/(sqrt(1.0-pow(v_rel,2)));
	Qmax=2.0*pow(gamma,2)*me*pow(v_rel,2)/(1.0+2.0*gamma*(me/mp)+pow(me/mp,2));// Qmax is the maximum energy transferred-> eV 
	v=sqrt(2.0*mp*me/((2.0*mp+2.0*me)*I))*v_rel;// Dimensionless -> reduced speed 

	T=Ep_p/lambda;
	wc=((4.0*Ep_p-2.0*sqrt(I*T))/I)-w2;
	wmax=Qmax/I;
		
	sigma_p=0.0;
    w=0.0;

	while(w<wmax){				
		dw=min(0.001*w,wmax-w);

		if(w==0.0){
			dw=0.001;
		}		
			
		sigma_p+=dw*Sp*(func_F1(v)+func_F2(v)*w)*pow(1.0+w,-3)/(1.0+exp(alpha*(w-wc)/v));	
		w+=dw;
	}

	return sigma_p;
}


/////////////////////////////////////////////////////////////////////////////
// Electron ionization differential cross-section and cross-section
/////////////////////////////////////////////////////////////////////////////

// Auxiliary function for ionization differential cross-section of electrons
double func_df_dw(double w){

	double c_df, d_df, e_df, f_df;
	c_df=1.1262;
	d_df=6.382;
	e_df=-7.8055;
	f_df=2.144;

	double df_dw=(c_df/pow(1.0+w,3))+(d_df/pow(1.0+w,4))+(e_df/pow(1.0+w,5))+(f_df/pow(1.0+w,6));
	return df_dw;
}

// Auxiliary function for ionization cross-section of electrons
double func_D(double t){

	double c_df, d_df, e_df, f_df;
	c_df=1.1262;
	d_df=6.382;
	e_df=-7.8055;
	f_df=2.144;

	double D=0.0;
	double u=(t+1.0)/2.0;
	D+=-c_df*(pow(u,-3)-1.0)/3.0;
	D+=-d_df*(pow(u,-4)-1.0)/4.0;
	D+=-e_df*(pow(u,-5)-1.0)/5.0;
	D+=-f_df*(pow(u,-6)-1.0)/6.0;
	D*=1.0/N;

	return D;
}

// Differential cross-section of electrons (RBEB binary-encounter-Bethe)
double func_d_sigma_e_E(double Ee_p, double Ee_s){

	double c_df, d_df, e_df, f_df, N_i;
	c_df=1.1262;
	d_df=6.382;
	e_df=-7.8055;
	f_df=2.144;
	N_i=(c_df/2.0)+(d_df/2.0)+(e_df/2.0)+(f_df/2.0);	

	double U_H2, B_H2,  u_primed, b_primed, beta_u, beta_b;	
	U_H2=39.603;// Average kinetic energy of orbital electron
	B_H2=I;// Binding energy
	u_primed=U_H2/me;
	b_primed=B_H2/me;
	beta_u=sqrt(1.0-(1.0/pow(1.0+u_primed,2)));
	beta_b=sqrt(1.0-(1.0/pow(1.0+b_primed,2)));

	double d_sigma_E, t, t_primed, w, beta_t;
	t=Ee_p/I;
	t_primed=Ee_p/me;
	beta_t=sqrt(1.0-(1.0/pow(1.0+t_primed,2)));	
	w=Ee_s/I;

	d_sigma_E=0.0;	

	d_sigma_E+=(((N_i/N)-2)/(t+1.0))*((1.0/(1+w))+(1.0/(t-w)))*((1.0+2.0*t_primed)/(pow(1.0+(t_primed/2.0),2)));
	d_sigma_E+=(2.0-(N_i/N))*((1.0/pow(1.0+w,2))+(1.0/pow(t-w,2))+(pow(b_primed,2)/pow(1.0+(t_primed/2.0),2)));
	d_sigma_E+=(1.0/(N*(1.0+w)))*func_df_dw(w)*(log(pow(beta_t,2)/(1.0-pow(beta_t,2))));
	d_sigma_E+=(1.0/(N*(1.0+w)))*func_df_dw(w)*(-pow(beta_t,2)-log(2.0*b_primed));
	d_sigma_E*=((Se/(2.0*b_primed))/(pow(beta_t,2)+pow(beta_u,2)+pow(beta_b,2)));
	d_sigma_E*=(1.0/I);	

	return d_sigma_E;
}

// Ionization cross-section for electrons
double func_sigma_e(double Ee_p){

	double c_df, d_df, e_df, f_df, N_i;
	c_df=1.1262;
	d_df=6.382;
	e_df=-7.8055;
	f_df=2.144;
	N_i=(c_df/2.0)+(d_df/2.0)+(e_df/2.0)+(f_df/2.0);

	double U_H2, B_H2,  u_primed, b_primed, beta_u, beta_b;	
	U_H2=39.603;// Average kinetic energy of orbital electron
	B_H2=I;// Binding energy
	u_primed=U_H2/me;
	b_primed=B_H2/me;
	beta_u=sqrt(1.0-(1.0/pow(1.0+u_primed,2)));
	beta_b=sqrt(1.0-(1.0/pow(1.0+b_primed,2)));	

	double sigma_e, t, t_primed, beta_t;
	t=Ee_p/I;
	t_primed=Ee_p/me;
	beta_t=sqrt(1.0-(1.0/pow(1.0+t_primed,2)));	

	sigma_e=0.0;

	sigma_e+=func_D(t)*(log(pow(beta_t,2)/(1.0-pow(beta_t,2)))-pow(beta_t,2)-log(2.0*b_primed));
	sigma_e+=(2.0-(N_i/N))*(1.0-(1.0/t)-(log(t)/(t+1))*((1.0+2.0*t_primed)/pow(1.0+(t_primed/2.0),2)));
	sigma_e+=(2.0-(N_i/N))*((pow(b_primed,2)/pow(1.0+(t_primed/2.0),2))*((t-1.0)/2.0));
	sigma_e*=((Se/(2.0*b_primed))/(pow(beta_t,2)+pow(beta_u,2)+pow(beta_b,2)));
	
	return sigma_e;
}


/////////////////////////////////////////////////////////////////////////////
// Secondary ionization 
/////////////////////////////////////////////////////////////////////////////

// Secondary ionization function for protons
double func_phi_p(double X_phi_p[], double Y_phi_p[], double Ep_p){

	int cord_E;
	double E=Ep_p;

	int N=int((pow(10.0,11)-pow(10.0,10))/pow(10.0,9));

	if(E>=pow(10.0,10)){
		cord_E=int((E-pow(10.0,10))/pow(10.0,9))+8.0*N;
	}
	if((E<pow(10.0,10)) && (E>=pow(10.0,9))){
		cord_E=int((E-pow(10.0,9))/pow(10.0,8))+7.0*N;
	}
	if((E<pow(10.0,9)) && (E>=pow(10.0,8))){
		cord_E=int((E-pow(10.0,8))/pow(10.0,7))+6.0*N;
	}
	if((E<pow(10.0,8)) && (E>=pow(10.0,7))){
		cord_E=int((E-pow(10.0,7))/pow(10.0,6))+5.0*N;
	}
	if((E<pow(10.0,7)) && (E>=pow(10.0,6))){
		cord_E=int((E-pow(10.0,6))/pow(10.0,5))+4.0*N;
	}
	if((E<pow(10.0,6)) && (E>=pow(10.0,5))){
		cord_E=int((E-pow(10.0,5))/pow(10.0,4))+3.0*N;
	}
	if((E<pow(10.0,5)) && (E>=pow(10.0,4))){
		cord_E=int((E-pow(10.0,4))/pow(10.0,3))+2.0*N;
	}
	if((E<pow(10.0,4)) && (E>=pow(10.0,3))){
		cord_E=int((E-pow(10.0,3))/pow(10.0,2))+N;
	}
	if((E<pow(10.0,3)) && (E>=pow(10.0,2))){
		cord_E=int((E-pow(10.0,2))/pow(10.0,1));
	}

	int cord_Emax=9.0*N;
	if(cord_E>=cord_Emax){
		cord_E=cord_Emax-2;
	}

	double phi_p=0.0;
	double A, B;
	double X1, X2, Y1, Y2;
	X1=X_phi_p[cord_E];
	X2=X_phi_p[cord_E+1];
	Y1=Y_phi_p[cord_E];
	Y2=Y_phi_p[cord_E+1];

	if(cord_E<=152){
		phi_p=0.0;
	}
	if(cord_E>152){
		A=log(Y2/Y1)/log(X2/X1);
		B=Y2/pow(X2,A);
		phi_p=B*pow(Ep_p,A);
	}

	return phi_p;
}

// Secondary ionization function for electrons
double func_phi_e(double X_phi_e[], double Y_phi_e[], double Ee_p){

	int cord_E;
	double E=Ee_p;

	int N=int((pow(10.0,11)-pow(10.0,10))/pow(10.0,9));

	if(E>=pow(10.0,10)){
		cord_E=int((E-pow(10.0,10))/pow(10.0,9))+8.0*N;
	}
	if((E<pow(10.0,10)) && (E>=pow(10.0,9))){
		cord_E=int((E-pow(10.0,9))/pow(10.0,8))+7.0*N;
	}
	if((E<pow(10.0,9)) && (E>=pow(10.0,8))){
		cord_E=int((E-pow(10.0,8))/pow(10.0,7))+6.0*N;
	}
	if((E<pow(10.0,8)) && (E>=pow(10.0,7))){
		cord_E=int((E-pow(10.0,7))/pow(10.0,6))+5.0*N;
	}
	if((E<pow(10.0,7)) && (E>=pow(10.0,6))){
		cord_E=int((E-pow(10.0,6))/pow(10.0,5))+4.0*N;
	}
	if((E<pow(10.0,6)) && (E>=pow(10.0,5))){
		cord_E=int((E-pow(10.0,5))/pow(10.0,4))+3.0*N;
	}
	if((E<pow(10.0,5)) && (E>=pow(10.0,4))){
		cord_E=int((E-pow(10.0,4))/pow(10.0,3))+2.0*N;
	}
	if((E<pow(10.0,4)) && (E>=pow(10.0,3))){
		cord_E=int((E-pow(10.0,3))/pow(10.0,2))+N;
	}
	if((E<pow(10.0,3)) && (E>=pow(10.0,2))){
		cord_E=int((E-pow(10.0,2))/pow(10.0,1));
	}

	int cord_Emax=9.0*N;
	if(cord_E>=cord_Emax){
		cord_E=cord_Emax-2;
	}

	double phi_e=0.0;
	double A, B;
	double X1, X2, Y1, Y2;
	X1=X_phi_e[cord_E];
	X2=X_phi_e[cord_E+1];
	Y1=Y_phi_e[cord_E];
	Y2=Y_phi_e[cord_E+1];

	A=log(Y2/Y1)/log(X2/X1);
	B=Y2/pow(X2,A);
	phi_e=B*pow(Ee_p,A);

	return phi_e;
}
