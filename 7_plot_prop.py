from cProfile import label
from cmath import tau
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rc("text",usetex=True)
import scipy as sp
import scipy.interpolate
import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable, ImageGrid
from matplotlib.ticker import MultipleLocator
from scipy.optimize import curve_fit

fs=22


mp=938.2720813e6 # eV
me=0.510998e6 # eV

def plot_jEn(pars):

    Z=pars[0]
    A=pars[1]
    gal=pars[2]
    mod=pars[3]
    nH2=str(int(pars[4]))
    if(pars[4]<1.0):
        nH2="0p"+str(int(pars[4]*10.0))

    fig=plt.figure(figsize=(10, 8))
    ax=plt.subplot(111)


    n=0
    E, j0, jb, jc, ja=np.loadtxt("./Prop_n/n_Z="+str(int(Z))+"_spectrum_"+gal+"_"+mod+"_"+nH2+".dat",unpack=True,usecols=[0,1,2,3,4])
    ax.plot(E,E**n*j0,'k-',linewidth=3.0,label=r'$j_0$')
    ax.plot(E,E**n*jb,'g--',linewidth=3.0,label=r'$j_{b}$')
    ax.plot(E,E**n*ja,'r:',linewidth=3.0,label=r'$j_{a}$')

    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim(1.0e5,1.0e10)

    if(Z==26.0):
        En_Fe_AMS, jEn_Fe_AMS=np.loadtxt("plot_data_flux_Fe_AMS.dat",unpack=True,usecols=[0,1])
        jEn_Fe_AMS*=1.0e-13/En_Fe_AMS**2.7
        En_Fe_AMS*=1.0e9

        En_Fe_Voyager, jEn_Fe_Voyager, err_En_Fe_Voyager, err_jEn_Fe_Voyager=np.loadtxt("plot_data_flux_Fe_Voyager.dat",unpack=True,usecols=[0,1,2,3])

        ax.plot(En_Fe_AMS,jEn_Fe_AMS,'go',label=r'$\textrm{AMS}$',markersize=10.0)
        ax.errorbar(En_Fe_Voyager,jEn_Fe_Voyager,err_jEn_Fe_Voyager,err_En_Fe_Voyager,'o',color='royalblue',markersize=9)
        ax.set_ylim(1.0e-16,5.0e-12)

    if(Z==1.0):
        ax.set_ylim(1.0e-12,1.0e-8)

    ax.set_xlabel(r'$E\,{\rm (eV)}$',fontsize=fs)
    ax.set_ylabel(r'$j(E)\,{\rm (eV^{-1}\, cm^{-2}\, s^{-1}\, sr^{-1})}$',fontsize=fs)
    for label_ax in (ax.get_xticklabels() + ax.get_yticklabels()):
        label_ax.set_fontsize(fs)
    ax.legend(loc='upper right', prop={"size":22})
    ax.grid(linestyle='--')

    plt.savefig("fg_jEn_Z="+str(int(Z))+"_"+gal+"_"+mod+"_"+nH2+".png")

def plot_jEn_dens(pars):

    Z=pars[0]
    A=pars[1]
    gal=pars[2]
    mod=pars[3]

    fig=plt.figure(figsize=(10, 8))
    ax=plt.subplot(111)


    n=0
    E, j0, ja_1e1=np.loadtxt("./Prop_n/n_Z="+str(int(Z))+"_spectrum_"+gal+"_"+mod+"_10.dat",unpack=True,usecols=[0,1,4])
    ja_1e2=np.loadtxt("./Prop_n/n_Z="+str(int(Z))+"_spectrum_"+gal+"_"+mod+"_100.dat",unpack=True,usecols=[4])
    ja_1e3=np.loadtxt("./Prop_n/n_Z="+str(int(Z))+"_spectrum_"+gal+"_"+mod+"_1000.dat",unpack=True,usecols=[4])

    if(gal=="LocISM"):
        ax.plot(E,E**n*j0,'r-',linewidth=3.0,label=r'{\rm VFe}')
        ax.plot(E,E**n*ja_1e1,'k:',linewidth=3.0,label=r'$N({\rm H}_2)=6\times 10^{20}\,{\rm cm^{-2}}$')
        ax.plot(E,E**n*ja_1e2,'k--',linewidth=3.0,label=r'$N({\rm H}_2)=6\times 10^{21}\,{\rm cm^{-2}}$')
        ax.plot(E,E**n*ja_1e3,'k-.',linewidth=3.0,label=r'$N({\rm H}_2)=6\times 10^{22}\,{\rm cm^{-2}}$')

    if(gal=="ScaleP"):
        ax.plot(E,E**n*j0,'g-',linewidth=3.0,label=r'{\rm VFe}')
        ax.plot(E,E**n*ja_1e1,'k:',linewidth=3.0,label=r'$N({\rm H}_2)=6\times 10^{20}\,{\rm cm^{-2}}$')
        ax.plot(E,E**n*ja_1e2,'k--',linewidth=3.0,label=r'$N({\rm H}_2)=6\times 10^{21}\,{\rm cm^{-2}}$')
        ax.plot(E,E**n*ja_1e3,'k-.',linewidth=3.0,label=r'$N({\rm H}_2)=6\times 10^{22}\,{\rm cm^{-2}}$')

    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim(1.0e5,1.0e10)

    if(mod=="diff"):
        ax.set_title(r'{\bf Diffusive Model}', fontsize=fs)
    if(mod=="ball"):
        ax.set_title(r'{\bf Ballistic Model}', fontsize=fs)

    if(Z==26.0):
        # En_Fe_AMS, jEn_Fe_AMS=np.loadtxt("plot_data_flux_Fe_AMS.dat",unpack=True,usecols=[0,1])
        # jEn_Fe_AMS*=1.0e-13/En_Fe_AMS**2.7
        # En_Fe_AMS*=1.0e9

        # En_Fe_Voyager, jEn_Fe_Voyager, err_En_Fe_Voyager, err_jEn_Fe_Voyager=np.loadtxt("plot_data_flux_Fe_Voyager.dat",unpack=True,usecols=[0,1,2,3])

        # ax.plot(En_Fe_AMS,jEn_Fe_AMS,'go',label=r'$\textrm{AMS}$',markersize=10.0)
        # ax.errorbar(En_Fe_Voyager,jEn_Fe_Voyager,err_jEn_Fe_Voyager,err_En_Fe_Voyager,'o',color='royalblue',markersize=9)
        ax.set_ylim(1.0e-16,5.0e-13)

    if(Z==1.0):
        ax.set_ylim(1.0e-12,1.0e-8)

    ax.set_xlabel(r'$E\,{\rm (eV)}$',fontsize=fs)
    ax.set_ylabel(r'$j(E)\,{\rm (eV^{-1}\, cm^{-2}\, s^{-1}\, sr^{-1})}$',fontsize=fs)
    for label_ax in (ax.get_xticklabels() + ax.get_yticklabels()):
        label_ax.set_fontsize(fs)
    ax.legend(loc='lower left', prop={"size":22})
    ax.grid(linestyle='--')

    plt.savefig("fg_jEn_prop_Z="+str(int(Z))+"_"+gal+"_"+mod+".png")

def plot_jE(pars):

    sCR=pars[0]
    gal=pars[1]
    mod=pars[2]
    nH2=str(int(pars[3]))
    if(pars[3]<1.0):
        nH2="0p"+str(int(pars[3]*10.0))

    fig=plt.figure(figsize=(10, 8))
    ax=plt.subplot(111)

    n=0
    E, j0, jb, jc, ja=np.loadtxt("./Prop_"+sCR+"/"+sCR+"_spectrum_"+gal+"_"+mod+"_"+nH2+".dat",unpack=True,usecols=[0,1,2,3,4])
    ax.plot(E,E**n*j0,'k-',linewidth=3.0,label=r'$j_0$')
    ax.plot(E,E**n*jb,'g--',linewidth=3.0,label=r'$j_{b}$')
    ax.plot(E,E**n*ja,'r:',linewidth=3.0,label=r'$j_{a}$')

    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim(1.0e3,1.0e10)
    if(sCR=="p"):
        ax.set_ylim(1.0e-12,1.0e-8)
    if(sCR=="e"):
        ax.set_ylim(1.0e-14,1.0e-2)
    ax.set_xlabel(r'$E\,{\rm (eV)}$',fontsize=fs)
    ax.set_ylabel(r'$j(E)\,{\rm (eV^{-1}\, cm^{-2}\, s^{-1}\, sr^{-1})}$',fontsize=fs)
    for label_ax in (ax.get_xticklabels() + ax.get_yticklabels()):
        label_ax.set_fontsize(fs)
    ax.legend(loc='upper right', prop={"size":22})
    ax.grid(linestyle='--')

    plt.savefig("fg_jE_"+sCR+"_"+gal+"_"+mod+"_"+nH2+".png")

def plot_ion(pars):

    gal=pars[0]
    mod=pars[1]

    fig=plt.figure(figsize=(10, 8))
    ax=plt.subplot(111)

    NH2, ion_p, ion_e=np.loadtxt("CR_xi_"+gal+"_"+mod+".dat",unpack=True,usecols=[0,1,2])
    ax.plot(NH2,ion_p,'r--',linewidth=3.0,label=r'$\zeta_{\rm e}({\rm H}_2)$')
    ax.plot(NH2,ion_e,'g-.',linewidth=3.0,label=r'$\zeta_{\rm e}({\rm H}_2)$')
    ax.plot(NH2,1.5*ion_p+ion_e,'k-',linewidth=3.0,label=r'$\zeta({\rm H}_2)$')


    filename='Data/data_Caselli.dat'
    NH2_Caselli, xi_Caselli, err_NH2_Caselli, err_xi_Caselli=np.loadtxt(filename,unpack=True,usecols=[0,1,2,3])
    ax.errorbar(NH2_Caselli,xi_Caselli,err_xi_Caselli,err_NH2_Caselli,'bo',markersize=10.0,elinewidth=2.5)#,label=r'{\rm Caselli}')

    filename='Data/data_Indriolo.dat'
    NH2_Indriolo, xi_Indriolo, err_NH2_Indriolo, err_xi_Indriolo=np.loadtxt(filename,unpack=True,usecols=[0,1,2,3])
    ax.errorbar(NH2_Indriolo,xi_Indriolo,err_xi_Indriolo,err_NH2_Indriolo,'ks',markersize=10.0,elinewidth=2.5)#,label=r'{\rm Indriolo}')

    filename='Data/data_upper_limit.dat'
    NH2_upper, xi_upper, err_NH2_upper, err_xi_upper=np.loadtxt(filename,unpack=True,usecols=[0,1,2,3])
    err_xi_upper*=0.0
    ax.errorbar(NH2_upper,xi_upper,0.5*xi_upper,err_NH2_upper,uplims=True,ecolor='orange',marker='s',mfc='orange',mec='orange',markersize=10.0,elinewidth=2.5,ls='none')#,label=r'{\rm Upper Limit}')

    filename='Data/data_Bialy.dat'
    NH2_upper, xi_upper, err_NH2_upper, err_xi_upper=np.loadtxt(filename,unpack=True,usecols=[0,1,2,3])
    err_xi_upper*=0.0
    ax.errorbar(NH2_upper,xi_upper,0.5*xi_upper,err_NH2_upper,uplims=True,ecolor='red',marker='s',mfc='red',mec='red',markersize=10.0,elinewidth=2.5,ls='none')#,label=r'{\rm Upper Limit}')

    filename='Data/data_Williams.dat'
    NH2_Williams, xi_Williams, err_NH2_Williams, err_xi_Williams=np.loadtxt(filename,unpack=True,usecols=[0,1,2,3])
    ax.errorbar(NH2_Williams,xi_Williams,err_xi_Williams,err_NH2_Williams,'g^',markersize=10.0,elinewidth=2.5)#,label=r'{\rm Williams}')

    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim(6.0e19,6.0e22)
    ax.set_ylim(6.0e-19,6.0e-15)
    ax.set_xlabel(r'$N({\rm H}_2)\,{\rm (cm^{-2})}$',fontsize=fs)
    ax.set_ylabel(r'$\zeta({\rm H}_2)\,{\rm (s^{-1})}$',fontsize=fs)
    for label_ax in (ax.get_xticklabels() + ax.get_yticklabels()):
        label_ax.set_fontsize(fs)
    ax.legend(loc='upper right', prop={"size":22})
    ax.grid(linestyle='--')

    plt.savefig("fg_ion_"+gal+"_"+mod+".png")

def plot_spu(pars):

    mod=pars[0]

    fig=plt.figure(figsize=(10, 8))
    ax=plt.subplot(111)

    NH2, ksp=np.loadtxt("CR_sp_LocISM_"+mod+".dat",unpack=True,usecols=[0,1])
    ax.plot(NH2,ksp,'r--',linewidth=3.0,label=r'${\rm VFe}$')

    NH2, ksp=np.loadtxt("CR_sp_Scalep_"+mod+".dat",unpack=True,usecols=[0,1])
    ax.plot(NH2,ksp,'g:',linewidth=3.0,label=r'${\rm S04}$')

    if(mod=="diff"):
        ax.set_title(r'{\bf Diffusive Model}', fontsize=fs)
    if(mod=="ball"):
        ax.set_title(r'{\bf Ballistic Model}', fontsize=fs)
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim(6.0e19,6.0e22)
    ax.set_ylim(1.0e-2,1.0e1)
    ax.set_xlabel(r'$N({\rm H}_2)\,{\rm (cm^{-2})}$',fontsize=fs)
    ax.set_ylabel(r'$Y^{\infty}_{\rm eff}({\rm H_2O})\,{\rm (molecules\, cm^{-2}\, s^{-1})}$',fontsize=fs)
    for label_ax in (ax.get_xticklabels() + ax.get_yticklabels()):
        label_ax.set_fontsize(fs)
    ax.legend(loc='upper right', prop={"size":22})
    ax.grid(linestyle='--')

    plt.savefig("fg_spu_"+mod+".png")

def plot_spu_ion(pars):

    mod=pars[0]

    fig=plt.figure(figsize=(10, 8))
    ax=plt.subplot(111)

    NH2, ion_p, ion_e=np.loadtxt("CR_xi_LocISM_"+mod+".dat",unpack=True,usecols=[0,1,2])
    ion = 1.5*ion_p+ion_e

    NH2, ksp=np.loadtxt("CR_sp_LocISM_"+mod+".dat",unpack=True,usecols=[0,1])
    ax.plot(NH2,ksp/ion,'r--',linewidth=3.0,label=r'${\rm VFe}$')

    NH2, ion_p, ion_e=np.loadtxt("CR_xi_LocISM_"+mod+".dat",unpack=True,usecols=[0,1,2])
    ion = 1.5*ion_p+ion_e

    NH2, ksp=np.loadtxt("CR_sp_Scalep_"+mod+".dat",unpack=True,usecols=[0,1])
    ax.plot(NH2,ksp/ion,'g:',linewidth=3.0,label=r'${\rm S04}$')

    if(mod=="diff"):
        ax.set_title(r'{\bf Diffusive Model}', fontsize=fs)
    if(mod=="ball"):
        ax.set_title(r'{\bf Ballistic Model}', fontsize=fs)
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlim(6.0e19,6.0e22)
    ax.set_ylim(1.0e15,5.0e17)
    ax.set_xlabel(r'$N({\rm H}_2)\,{\rm (cm^{-2})}$',fontsize=fs)
    ax.set_ylabel(r'$Y_{\rm eff}^{\infty}/\zeta({\rm H}_2)\,{\rm (molecules\, cm^{-2})}$',fontsize=fs)
    for label_ax in (ax.get_xticklabels() + ax.get_yticklabels()):
        label_ax.set_fontsize(fs)
    ax.legend(loc='upper right', prop={"size":22})
    ax.grid(linestyle='--')

    plt.savefig("fg_spu_ion_"+mod+".png")

# ## Plot propagted spectra
# pars_p=["p","LocISM","diff",200.0]
# plot_jE(pars_p)

# pars_e=["e","LocISM","diff",200.0]
# plot_jE(pars_e)

# pars_p=["p","LocISM","ball",200.0]
# plot_jE(pars_p)

# pars_e=["e","LocISM","ball",200.0]
# plot_jE(pars_e)

# ## Plot ionization rate
# plot_ion(["LocISM","diff"])
# plot_ion(["LocISM","ball"])

## Plot propagted spectra
pars_Fe=[26.0,56.0,"LocISM","diff",200.0]
plot_jEn(pars_Fe)

pars_Fe=[26.0,56.0,"LocISM","ball",200.0]
plot_jEn(pars_Fe)

## Plot propagted spectra for different column densities
pars_Fe=[26.0,56.0,"LocISM","diff"]
plot_jEn_dens(pars_Fe)

pars_Fe=[26.0,56.0,"LocISM","ball"]
plot_jEn_dens(pars_Fe)

pars_Fe=[26.0,56.0,"ScaleP","diff"]
plot_jEn_dens(pars_Fe)

pars_Fe=[26.0,56.0,"ScaleP","ball"]
plot_jEn_dens(pars_Fe)

## Plot supttering rate
plot_spu(["diff"])
plot_spu(["ball"])

## Plot supttering rate
plot_spu_ion(["diff"])
plot_spu_ion(["ball"])
