#include <iostream>
#include <complex>
#include <math.h>
#include <fstream>
#include <string>
#include <iomanip>
#include "pack_Xion.h"
#include "pack_prop.h"

using namespace std;

const double pi=3.14159265359;
const double Lc=10.0;// pc

int main(int argc, char* argv[]){

	ifstream input;
    input.open("pars_prop.dat");
    double Enmin, Enmax, dlogE, dlogE_smooth;
    double Z, A, alpha;

    input >> Enmin >> Enmax >> dlogE >> dlogE_smooth;
    input >> Z >> A >> alpha;

    int NE=int(log10(Enmax/Enmin)/dlogE)+1;

	// Array to store the CR spectra.
    double *Y_Jn=new double[NE];

    // Array to store stopping power.
    double *arr_Ys=new double[NE];

    string gal_name="ScaleP";

    string pref_in, suf_i, pref_o1, pref_o2, suf_o;
	pref_in="./Prop_n/n_Z="+to_string(int(Z))+"_spectrum";
    pref_o1="CR_sp";
    pref_o2="d_CR_sp";
    suf_o=".dat";
    
	int N_nH2=13;
    double nH2[13]={0.2, 0.5, 1.0, 2.0, 5.0, 10.0, 20.0, 50.0, 100.0, 200.0, 500.0, 1000.0, 2000.0};
    
    string tag_nH2, tag_mod;

	// Read input arguments from command line.
    if(argc==2){
        tag_mod=argv[1];// Input diff or ball
    } 
    else{
        tag_mod="diff";
    }

	cout << "Model for CR transport in clouds: " << tag_mod << endl;
	tag_mod="_"+tag_mod;
      
    ifstream inputCR_n[N_nH2];
	string inputCR_n_name; 

	ofstream output1, output2, output3, output4;
    string output1_name, output2_name, output3_name, output4_name;
	output1_name=pref_o1+"_"+gal_name+tag_mod+".dat";
	output2_name=pref_o2+"_"+gal_name+tag_mod+"_20.dat";
	output3_name=pref_o2+"_"+gal_name+tag_mod+"_200.dat";
	output4_name=pref_o2+"_"+gal_name+tag_mod+"_2000.dat";

	output1.open(output1_name.c_str());
	output2.open(output2_name.c_str());
	output3.open(output3_name.c_str());
	output4.open(output4_name.c_str());
    	
    int count;
    double En;

	for(int i=0;i<NE;i++){
		En=Enmin*pow(10.0,i*dlogE);
		arr_Ys[i]=alpha*pow(func_Se(En)*1.0e15,2);
		// arr_Ys[i]=alpha*pow(0.05*func_LEn(Z,A,En),2);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	double dEn, J0n, Jbn, Jcn, Jan, Ys, ksp;

	cout << "The file name: " << endl;
	cout << " " << endl;

	for(int n=0;n<N_nH2;n++){
		tag_nH2=to_string(int(nH2[n]));
		if(nH2[n]<1.0){
    		tag_nH2="0p"+to_string(int(nH2[n]*10.0));
	    }
		if(nH2[n]<0.1){
    		tag_nH2="0p0"+to_string(int(nH2[n]*100.0));
	    }
		
		inputCR_n_name=pref_in+"_"+gal_name+tag_mod+"_"+tag_nH2+".dat";

		cout << inputCR_n_name << endl; 
		cout << " " << endl;

		// Spectrum of CRs in the enrgy range from 10^3 to 10^10    
		inputCR_n[n].open(inputCR_n_name.c_str());
		
		count=0;
	    while(inputCR_n[n] >> En >> J0n >> Jbn >> Jcn >> Jan){
        	Y_Jn[count]=Jan;// (eV/n)^-1 cm^-2 s^-1 sr^-1
        	count+=1;
    	}

		ksp=0.0;// Effective sputtering yield.

		for(int i=0;i<NE-1;i++){
			En=Enmin*pow(10.0,i*dlogE);
			dEn=Enmin*(pow(10.0,(i+1)*dlogE)-pow(10.0,i*dlogE));
			
			// Intensity of CRs with respect to kinetic energy.
			Jan=Y_Jn[i];// (eV/n)^-1 cm^-2 s^-1 sr^-1 

			// Yield at a particular energy (dimensionless).
			Ys=arr_Ys[i];

            if(En>=1.0e5){
                // Note that Jp is in unit (eV/n)^-1 cm^-2 s^-1 sr^-1.
                ksp+=2.0*4.0*pi*Jan*Ys*dEn;
            }

			if(nH2[n]==20.0){		
				output2 << En;
				output2 << " " << 4.0*pi*En*Jan*Ys;
				output2 << endl;
			}

			if(nH2[n]==200.0){		
				output3 << En;
				output3 << " " << 4.0*pi*En*Jan*Ys;
				output3 << endl;
			}

			if(nH2[n]==2000.0){		
				output4 << En;
				output4 << " " << 4.0*pi*En*Jan*Ys;
				output4 << endl;
			}
		}

        // Note that we want the yield per incoming ions and thus have to divide by the fluence.
	    output1 << nH2[n]*Lc*3.08567758e18 << " " << ksp << endl;

		inputCR_n[n].close();
	}
      
	output1.close();
    output2.close();
	output3.close();
    output4.close();
	input.close();
}
